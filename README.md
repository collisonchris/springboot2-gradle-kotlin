## springboot2-gradle-kotlin

A sample project for running kotlin & java with springboot2 & building with gradle 5

* Clone the project locally
* Import into Intellij: File -> New -> Project From Existing Sources
* Import from external model -> Gradle

#### Getting tests running in Intellij

It appears the built in gradle 5 task executor for Intellij is buggy(https://youtrack.jetbrains.com/issue/IDEA-221159) so you can switch it to running using the Intellij runtime & it executes fine

Build, Execution, Deployment -> Build Tools -> Gradle -> Run tests using from Gradle to IntelliJ IDEA

### Running tests on cli

`./gradlew clean test`