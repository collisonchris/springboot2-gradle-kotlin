package com.aimia.app

import com.aimia.app.domain.AccountMergeEntityGroup
import spock.lang.Specification
import spock.lang.Unroll

class RuleFunctionTest extends Specification {

    RuleFunctions ruleFunctions = new RuleFunctions()

    @Unroll
    def "test valid account merge entity group"() {
        when:
        Boolean result = ruleFunctions.isValidAccountMergeEntityGroup(emeg)
        then:
        result == expected

        where:
        expected | emeg
        false     | new AccountMergeEntityGroup()
        true     | new AccountMergeEntityGroup(programCode: "ABCDE")
        true     | new AccountMergeEntityGroup(programCode: "ABC")
        false    | new AccountMergeEntityGroup(programCode: "ABCDEF")
    }

    def "test program code function"() {
        when:
        Boolean result = ruleFunctions.hasValidProgramCode(programCode)
        then:
        result == expected

        where:
        expected | programCode
        false    | null
        false    | ""
        true     | "ABC"
        true     | "ABCD"
        false     | "ABCDEF"
    }

    @Unroll
    def "test entity group code function"() {
        when:
        Boolean result = ruleFunctions.hasValidEntityGroupCode(entityGroupCode)
        then:
        result == expected

        where:
        expected | entityGroupCode
        false    | null
        false    | ""
        false    | "TACO"
        true     | "PB"
        true     | "PBTACO"
        true     | "PBTYPEANYTHINGELSE%"
    }
}
