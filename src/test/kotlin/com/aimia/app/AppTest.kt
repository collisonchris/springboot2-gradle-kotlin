package com.aimia.app

import com.aimia.app.controller.SimpleController
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.http.HttpStatus

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AppTest(@Autowired val restTemplate: TestRestTemplate, @Autowired val simpleController: SimpleController) {

    @Test
    fun contextLoads() {
        assertThat(simpleController).isNotNull
    }

    /**
     * This Test hits the / binding which is in our SimpleController.kt
     */
    @Test
    fun basicRestRequest() {
        val entity = restTemplate.getForEntity<String>("/")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body).isEqualTo("""{"id":1,"content":"blah"}""")
    }


    /**
     * This Test hists the Java HelloController.java
     */
    @Test
    fun basicJavaRestRequest() {
        val entity = restTemplate.getForEntity<String>("/java")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body).isEqualTo("""some string""")
    }

}