package com.aimia.app.service

import com.aimia.app.dao.SimpleDao
import com.aimia.app.domain.AccountMergeEntityGroup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SimpleKotlinService {

    @Autowired
    lateinit var simpleDao: SimpleDao

    fun DoSomeBusinessLogic(): List<AccountMergeEntityGroup> {
        return simpleDao.getSomeData()
    }

}