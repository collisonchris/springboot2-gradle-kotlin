package com.aimia.app.controller

import com.aimia.app.domain.SimpleObject
import com.aimia.app.service.SimpleKotlinService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

@RestController
class SimpleController {

    val counter = AtomicLong()

    @Autowired
    lateinit var simpleKotlinService: SimpleKotlinService

    @GetMapping("/greeting")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String) =
            SimpleObject(counter.incrementAndGet(), "Hello, $name").toString()

    @GetMapping("/")
    fun slash() =
            run {
                println("hit slash")
                SimpleObject(1,"blah")
            }

    @GetMapping("/db")
    fun db() = simpleKotlinService.DoSomeBusinessLogic()
}


