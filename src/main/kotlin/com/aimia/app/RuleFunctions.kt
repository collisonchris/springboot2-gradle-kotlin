package com.aimia.app

import com.aimia.app.domain.AccountMergeEntityGroup

class RuleFunctions {
    fun isValidAccountMergeEntityGroup(ameg : AccountMergeEntityGroup) : Boolean {
        if(hasValidProgramCode(ameg.programCode)) {
            return true
        }
        return false
    }

    fun hasValidProgramCode(programCode : String?) : Boolean {
        if(!programCode.isNullOrEmpty() && programCode.length <= 5) {
            return true
        }
        return false
    }

    fun hasValidEntityGroupCode(entityGroupCode : String?) : Boolean {
       if(!entityGroupCode.isNullOrEmpty() && entityGroupCode.startsWith("PB")) {
           return true
       }
       return false
    }
}

