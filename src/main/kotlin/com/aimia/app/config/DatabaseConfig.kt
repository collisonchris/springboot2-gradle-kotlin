package com.aimia.app.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType
import javax.sql.DataSource

@Configuration
class DatabaseConfig {

    @Bean(name = ["dataSource"])
    fun dataSource(): DataSource {
        val hkConfig = HikariConfig()
        hkConfig.jdbcUrl = "jdbc:mysql://localhost:3306/ods"
        hkConfig.username = "root"
        hkConfig.password = "password"
        return HikariDataSource(hkConfig) as DataSource
    }

    @Bean
    fun jdbcTemplate(@Qualifier("dataSource") dataSource: DataSource): JdbcTemplate {
        return JdbcTemplate(dataSource)
    }
}