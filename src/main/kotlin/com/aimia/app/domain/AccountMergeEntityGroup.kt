package com.aimia.app.domain

data class AccountMergeEntityGroup(val id: Int = Int.MIN_VALUE,
                                   var entityGroupCode: String = "",
                                   var entityGroupDesc: String = "",
                                   var programCode: String = "",
                                   val isActive: Boolean = false,
                                   val isDisplay: Boolean = false)