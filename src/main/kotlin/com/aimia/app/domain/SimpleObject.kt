package com.aimia.app.domain

data class SimpleObject(val id: Long, val content: String)