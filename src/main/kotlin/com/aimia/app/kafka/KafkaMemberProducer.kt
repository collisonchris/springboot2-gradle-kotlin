package com.aimia.app.kafka

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.support.SendResult
import org.springframework.stereotype.Service
import org.springframework.util.concurrent.ListenableFutureCallback


@Service
class KafkaMemberProducer {

    @Value(value = "\${kafka.members.topic}")
    val topic: String? = null

    @Autowired
    lateinit var kafkaTemplate : KafkaTemplate<String, String>

    fun sendMessage(key: Int, message: String) {

        val future = kafkaTemplate.send(topic as String, key.toString(), message)

        future.addCallback(object : ListenableFutureCallback<SendResult<String, String>> {

            override fun onSuccess(result: SendResult<String, String>?) {
                println("Sent key=[$key] message=[$message] with offset=[" + result!!.getRecordMetadata().offset() + "]")
            }

            override fun onFailure(ex: Throwable) {
                println("Unable to send key[$key] due to : " + ex.message)
            }
        })
    }
}