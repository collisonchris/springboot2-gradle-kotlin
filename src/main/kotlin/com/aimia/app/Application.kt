package com.aimia.app

import com.aimia.app.dao.InteractionDao
import com.aimia.app.dao.MemberDao
import com.aimia.app.dao.TransactionDao
import com.aimia.app.kafka.KafkaInteractionsProducer
import com.aimia.app.kafka.KafkaMemberProducer
import com.aimia.app.kafka.KafkaTransactionProducer
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.SpringApplication


@SpringBootApplication
class Application
    fun main(args: Array<String>) {
        val context = SpringApplication.run(Application::class.java, *args)

        val memberProducer = context.getBean(KafkaMemberProducer::class.java)
        val interactionsProducer = context.getBean(KafkaInteractionsProducer::class.java)
        val transactionProducer = context.getBean(KafkaTransactionProducer::class.java)
        val memberDao = context.getBean(MemberDao::class.java)
        val interactionsDao = context.getBean(InteractionDao::class.java)
        val transactionDao = context.getBean(TransactionDao::class.java)
        val objectMapper = context.getBean(ObjectMapper::class.java)

        val members = memberDao.getAllMembers()

        members.forEach { memberProducer.sendMessage(it["id"] as Int, objectMapper.writeValueAsString(it)) }

        val interactions = interactionsDao.getAllInteractions()

        interactions.forEach { interactionsProducer.sendMessage(it["interaction_id"] as Int, objectMapper.writeValueAsString(it)) }

        val transactions = transactionDao.getAllTransactions()

        transactions.forEach { transactionProducer.sendMessage(it["id"] as Int, objectMapper.writeValueAsString(it)) }

        context.close()
}
