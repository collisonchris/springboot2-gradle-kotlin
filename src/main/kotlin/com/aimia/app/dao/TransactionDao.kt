package com.aimia.app.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet

@Repository
class TransactionDao {

    companion object {
        var transactionsQuery = "select rt.*, rc.code " +
                "from recs_transaction rt, recs_currency rc where " +
                "rt.currency_id_ext = rc.id"
    }

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    fun getAllTransactions() :List<Map<String,Any>> = jdbcTemplate.query(transactionsQuery) {
        rs: ResultSet, _: Int ->
        mutableMapOf("id" to rs.getInt("id"),
                "interaction_id" to rs.getInt("interaction_id"),
                "currency_code" to rs.getString("code"),
                "amount" to rs.getInt("amount"),
                "transaction_type" to rs.getString("transaction_type"),
                "issuing_partner" to rs.getString("issuing_partner"),
                "transaction_date" to rs.getDate("transaction_date"),
                "posting_date" to rs.getDate("posting_date"))
    }
}