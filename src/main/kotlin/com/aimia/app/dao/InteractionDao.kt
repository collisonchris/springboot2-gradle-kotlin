package com.aimia.app.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet

@Repository
class InteractionDao {

    companion object {
        var interactionsQuery = "select i.*, ip.content, id.summary " +
                "from recs_interaction i, recs_interaction_payload ip, recs_interaction_description id " +
                "where i.id = ip.interaction_id " +
                "and i.description_id = id.id"
    }

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    fun getAllInteractions() : List<Map<String,Any>> = jdbcTemplate.query(interactionsQuery) {
        rs: ResultSet, _: Int ->
        mutableMapOf("interaction_id" to rs.getInt("id"),
                "member_id" to rs.getInt("member_id"),
                "interaction_date" to rs.getDate("interaction_date"),
                "interaction_type" to rs.getString("interaction_type"),
                "interaction_summary" to rs.getString("summary"),
                "interaction_classifier" to rs.getString("interaction_classifier"),
                "interaction_payload" to rs.getString("content"))
    }
}