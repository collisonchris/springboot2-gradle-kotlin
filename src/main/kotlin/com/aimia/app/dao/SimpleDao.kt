package com.aimia.app.dao

import com.aimia.app.domain.AccountMergeEntityGroup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet

@Repository
class SimpleDao {

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    fun getSomeData(): List<AccountMergeEntityGroup> = jdbcTemplate.query("select * from ods.account_merge_entity_group") { rs : ResultSet, _: Int ->
        AccountMergeEntityGroup(rs.getInt("id"), rs.getString("entity_group_code"), rs.getString("entity_group_desc"),rs.getString("program_code") ?: "" , rs.getBoolean("is_active"), rs.getBoolean("is_display"))
    }

}