package com.aimia.app.dao

import com.aimia.app.domain.AccountMergeEntityGroup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet

@Repository
class MemberDao {

    companion object {
        val membersQuery = "select m.*, st.`code` from ods.me_members m, ods.me_member_status st where m.member_status_id = st.id"
        val membersByProgramQuery = "select m.*, st.`code` from ods.me_members m, ods.me_member_status st where m.member_status_id = st.id and m.program_code = ?"
        val memberProfileQuery = "select m.member_id, m.salutation,m.first_name,m.last_name,m.middle_name,m.suffix,m.date_of_birth,m.gender,m.enrolment_date," +
                "m.member_status_id,ms.code as member_status, mst.member_state_id, mtt.code, mt.primary_token_value,mpa.question_code, mpav.answer_value " +
                " from ods.me_members m join ods.me_member_status ms on m.member_status_id=ms.id " +
                " left join ods.me_member_states mst on m.member_id=mst.member_id " +
                " left join ods.me_member_tokens mt  on m.member_id=mt.member_id left join ods.me_token_type mtt on mt.token_type_id=mtt.id " +
                " left join ods.me_profile_answers mpa on mpa.member_id=m.member_id " +
                " left join ods.me_profile_answer_values mpav on mpav.profile_answer_id=mpa.id"
    }

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    fun getAllMembers(): List<Map<String,Any>> = jdbcTemplate.query(membersQuery) { rs: ResultSet, _: Int ->
        mutableMapOf("id" to rs.getInt("member_id"),
                "first_name" to rs.getString("first_name"),
                "last_name" to rs.getString("last_name"),
                "enrollment_date" to rs.getDate("enrolment_date"),
                "program_code" to rs.getString("program_code"),
                "suffix" to rs.getString("suffix"),
                "date_of_birth" to rs.getDate("date_of_birth"),
                "gender" to rs.getString("gender"),
                "status_code" to rs.getString("code"))
    }

//    fun getMemberProfiles() : List<Map<String,Any>> = jdbcTemplate.query(memberProfileQuery) { rs: ResultSet, _: Int ->
//        mutableMapOf("id" to rs.getInt("member_id"),
//                "salutation" to rs.getInt("salutation"),
//                "first_name" to rs.getInt("first_name"),
//                "middle_name" to rs.getInt("middle_name"),
//                "last_name" to rs.getInt("last_name"),
//                "suffix" to rs.getInt("suffix"),
//                "date_of_birth" to rs.getInt("date_of_birth"),
//                "gender" to rs.getInt("gender"),
//                "status_code" to rs.getInt("member_status"),
//                "id" to rs.getInt("member_id"))
//    }

}