package com.aimia.app.controller;


import com.aimia.app.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @Autowired
    GenericService genericService;

    @RequestMapping("/java")
    public @ResponseBody String hello() {
        return genericService.DoWork();
    }

    @RequestMapping("/java-greeting")
    public @ResponseBody String greeting(@RequestParam String name) {
        return "Hello " + name;
    }

}
